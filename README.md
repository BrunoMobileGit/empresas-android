![N|Solid](logo_ioasys.png)

# SOBRE #

Este projeto permite realizar a autenticação de um usuário, pesquisar, listar e detalhar empresas. Com mais tempo para o desenvolvimento
do projeto iriar adicionar mais funcionalidades ao sistema como: "Capturar a sessão de Login", "Implementar o Room para armazenar as empresas",
"Desenvolver uma função para verificar Conectividade de Internet", "Realizar mais testes unitários", "Adicionar Animações".

# Instruções #
1 - Realize o login.
2 - Pesquise pelo nome de uma empresa.
3 - Selecione a empresa e verifique os detalhes.

## Layout mobile
![N|Solid](img2.png)
![N|Solid](img3.png)
![N|Solid](img4.png)
![N|Solid](img1.png)

# Bibliotecas Utilizadas
## Retrofit
Responsável por consumir a api do projeto para a realização do login, listagem e detalhes das empresas.

## Glide
Utilizado para realizar o load da imagem da empresa.

## Navegation Component
Utilizado para navegação de fragmentos no projeto entre o Search e Details

## Dagger - Hilt
Utilizado como Injeção de dependência no projeto por ser uma biblioteca de fácil implementação e por eliminar 
a criação de várias classes manualmente.

## Data Binding
Responsável por vincular o "Model Enterprise" ao Layout XML, afim de exibir todos os detalhes das Empresas. 

## Coroutines
Como o projeto faz consumo de Api é necessario aguardar a resposta das consultas-endpoints de forma que
a aplicação funcione de forma suave.

## Lifecycle
Como o projeto utiliza o livedata para armazenar as empresas pesquisadas e observar o seu estado em tempo real, foi
usado o lifecycleOwner.

## RecyclerView
Responsável pela exibição das empresas pesquisadas.

## Gson
Responsável por converter o Json de retorno da Api para Objetos Kotlin
 
## Mockk
Biblioteca utilizada para realização de testes no projeto. Essencial para dar mocked em objetos e classes como o "Repository"
e "Usecases".