package com.example.empresas_android.presentation

import android.app.Application
import com.example.empresas_android.domain.usecases.PostLoginUseCase
import com.example.empresas_android.presentation.ui.main.MainViewModel
import io.mockk.mockk
import org.junit.Assert
import org.junit.Test

class MainViewModelTest {

    private val postLoginUseCase = mockk<PostLoginUseCase>()

    private val mainViewModel = MainViewModel(postLoginUseCase, application = Application())

    @Test
    fun `check null fields`() {

        val email = ""
        val password = ""

        val result = mainViewModel.checkNullFields(email, password)

        Assert.assertEquals(result, true)

    }

    @Test
    fun `check filled fields`() {

        val email = "emailfield"
        val password = "passwordfield"

        val result = mainViewModel.checkNullFields(email, password)

        Assert.assertEquals(result, false)

    }

}