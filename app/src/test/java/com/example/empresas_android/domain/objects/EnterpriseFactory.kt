package com.example.empresas_android.domain.objects

import com.example.empresas_android.data.model.Enterprise
import com.example.empresas_android.data.model.EnterpriseType

object EnterpriseFactory {

    val enterpriseType = EnterpriseType(
        "IT",
        7
    )

    val enterprises = listOf(

        Enterprise(
            "Alton",
            "UK",
            "We are reinventing the sales channel through our " +
                    "bespoke platform, myvendorlink, which aims to bring the " +
                    "best niche, new and emerging" +
                    " technology directly to IT resellers - and vice versa!",
            "null",
            "VendorLink",
            enterpriseType,
            "null",
            4,
            "null",
            false,
            "null",
            "/uploads/enterprise/photo/4/240.jpeg",
            5000.0,
            "null",
            0

        )

    )
}