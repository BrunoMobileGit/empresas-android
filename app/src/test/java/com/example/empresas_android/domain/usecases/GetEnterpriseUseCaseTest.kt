package com.example.empresas_android.domain.usecases

import com.example.empresas_android.domain.objects.EnterpriseFactory
import com.example.empresas_android.domain.repository.Repository
import com.example.empresas_android.domain.usecases.GetEnterpriseUseCase
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Test

class GetEnterpriseUseCaseTest {

    private val repository = mockk<Repository>()
    private val getEnterpriseUseCase = GetEnterpriseUseCase(repository)

    @Test
    fun `getEnterprise return list with success`() = runBlocking {

        val name = "VendorLink"

        coEvery { repository.getEnterprise(name) } returns EnterpriseFactory.enterprises

        val result = getEnterpriseUseCase.getEnterprise(name)

        Assert.assertEquals(result, EnterpriseFactory.enterprises)

    }

    @Test
    fun `getEnterprise return list empty`() = runBlocking {

        val name = "VendorLink"

        coEvery { repository.getEnterprise(name) } returns null

        val result = getEnterpriseUseCase.getEnterprise(name)

        Assert.assertEquals(result, null)

    }

}