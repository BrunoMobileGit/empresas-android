package com.example.empresas_android.domain.usecases

import com.example.empresas_android.domain.objects.InvestorFactory
import com.example.empresas_android.domain.repository.Repository
import com.example.empresas_android.domain.usecases.PostLoginUseCase
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Test

class PostLoginUseCaseTest {

    private val repository = mockk<Repository>()
    private val postLoginUseCase = PostLoginUseCase(repository)

    @Test
    fun `postLogin return with sucess`() = runBlocking {

        val email = "testeapple@ioasys.com.br"
        val password = "12341234"

        coEvery { repository.postLogin(email, password) } returns InvestorFactory.success

        val result = postLoginUseCase.postLogin(email, password)

        Assert.assertEquals(result, InvestorFactory.success)

    }

}