package com.example.empresas_android.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Enterprise(
    val city: String,
    val country: String,
    val description: String,
    val email_enterprise: String,
    val enterprise_name: String,
    val enterprise_type: EnterpriseType,
    val facebook: String,
    val id: Int,
    val linkedin: String,
    val own_enterprise: Boolean,
    val phone: String,
    val photo: String,
    val share_price: Double,
    val twitter: String,
    val value: Int
) : Parcelable