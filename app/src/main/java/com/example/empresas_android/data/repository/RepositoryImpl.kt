package com.example.empresas_android.data.repository

import com.example.empresas_android.data.model.Enterprise
import com.example.empresas_android.data.service.MyInterceptor.Companion.ACCESS_TOKEN
import com.example.empresas_android.data.service.MyInterceptor.Companion.CLIENT
import com.example.empresas_android.data.service.MyInterceptor.Companion.UID
import com.example.empresas_android.data.service.RequestApi
import com.example.empresas_android.domain.repository.Repository
import javax.inject.Inject

class RepositoryImpl @Inject constructor(
    var requestApi: RequestApi
) : Repository {

    override suspend fun postLogin(email: String, password: String): Boolean {

        val response = requestApi.postLogin(email, password)
        val body = response?.body()
        if (body?.success == true) {
            ACCESS_TOKEN = response?.headers()["access-token"].toString()
            CLIENT = response?.headers()["client"].toString()
            UID = response?.headers()["uid"].toString()
            return true
        }
        return false
    }

    override suspend fun getEnterprise(name: String): List<Enterprise> {
        lateinit var enterpriseList: List<Enterprise>

        try {
            val response = requestApi.searchCompany(name)
            val body = response?.body()
            if (body != null) {
                enterpriseList = body.enterprises
            }
        } catch (exception: Exception) {
            enterpriseList = listOf()
        }
        return enterpriseList
    }

}