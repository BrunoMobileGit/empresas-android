package com.example.empresas_android.data.model


import com.google.gson.annotations.SerializedName

data class InvestorResponse(
    @SerializedName("success")
    val success: Boolean
)