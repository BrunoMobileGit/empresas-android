package com.example.empresas_android.data.model

data class EnterprisesResponse(
    val enterprises: List<Enterprise>
)