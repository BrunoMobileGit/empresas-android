package com.example.empresas_android.data.service

import com.example.empresas_android.data.model.EnterprisesResponse
import com.example.empresas_android.data.model.InvestorResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface RequestApi {

    @POST("users/auth/sign_in")
    suspend fun postLogin(
        @Query("email") email: String,
        @Query("password") password: String
    ): Response<InvestorResponse>?

    @GET("enterprises")
    suspend fun searchCompany(
        @Query("name") name: String
    ): Response<EnterprisesResponse>?

}