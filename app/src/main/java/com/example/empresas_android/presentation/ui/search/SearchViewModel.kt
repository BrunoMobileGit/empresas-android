package com.example.empresas_android.presentation.ui.search

import android.app.Application
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.example.empresas_android.data.model.Enterprise
import com.example.empresas_android.domain.usecases.GetEnterpriseUseCase
import kotlinx.coroutines.launch

class SearchViewModel @ViewModelInject constructor(
    private val getEnterpriseUseCase: GetEnterpriseUseCase,
    application: Application,
) : AndroidViewModel(application) {

    val enterprises: MutableLiveData<List<Enterprise>> = MutableLiveData()

    suspend fun getEnterprise(name: String) {
        viewModelScope.launch {
            enterprises.value = getEnterpriseUseCase.getEnterprise(name)
        }

    }
}