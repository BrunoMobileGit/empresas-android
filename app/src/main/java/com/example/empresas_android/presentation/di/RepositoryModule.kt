package com.example.empresas_android.presentation.di

import com.example.empresas_android.data.repository.RepositoryImpl
import com.example.empresas_android.data.service.RequestApi
import com.example.empresas_android.domain.repository.Repository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object RepositoryModule {

    @Singleton
    @Provides
    fun providerRepository(
        requestApi: RequestApi
    ): Repository {
        return RepositoryImpl(requestApi)
    }

}