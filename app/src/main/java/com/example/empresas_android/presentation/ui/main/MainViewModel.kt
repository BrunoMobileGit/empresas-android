package com.example.empresas_android.presentation.ui.main

import android.app.Application
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.AndroidViewModel
import com.example.empresas_android.domain.usecases.PostLoginUseCase

class MainViewModel @ViewModelInject constructor(
    private val postLoginUseCase: PostLoginUseCase,
    application: Application
) : AndroidViewModel(application) {

    suspend fun postLogin(email: String, password: String): Boolean =
        postLoginUseCase.postLogin(email, password)

    fun checkNullFields(email: String, senha: String): Boolean {
        if (email.trim().isEmpty() || senha.trim().isEmpty()) {
            return true
        }
        return false
    }

}