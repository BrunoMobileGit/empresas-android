package com.example.empresas_android.presentation.ui.main

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.lifecycle.lifecycleScope
import com.example.empresas_android.R
import com.example.empresas_android.databinding.ActivityMainBinding
import com.example.empresas_android.presentation.ui.enterprise.EnterpriseActivity
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    val mainViewModel: MainViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val bindingMain = ActivityMainBinding.inflate(layoutInflater)
        setContentView(bindingMain.root)

        bindingMain.apply {

            btLogin.setOnClickListener {
                bindingMain.progressBar.visibility = View.VISIBLE
                if (mainViewModel.checkNullFields(
                        editEmail.text.toString(),
                        editSenha.text.toString()
                    )
                ) {
                    bindingMain.progressBar.visibility = View.INVISIBLE
                    Response.error = getString(R.string.main_error_field)
                    btLogin.setBackgroundResource(R.color.lightMediumGray)
                } else {
                    lifecycleScope.launch {
                        val resultLogin = mainViewModel.postLogin(
                            editEmail.text.toString(),
                            editSenha.text.toString()
                        )
                        if (resultLogin) {
                            startActivity(Intent(this@MainActivity, EnterpriseActivity::class.java))
                            finish()
                        } else {
                            btLogin.setBackgroundResource(R.color.lightMediumGray)
                            bindingMain.progressBar.visibility = View.INVISIBLE
                            Response.error =
                                getString(R.string.main_error_credentials)
                        }

                    }

                }
            }

        }

    }

}