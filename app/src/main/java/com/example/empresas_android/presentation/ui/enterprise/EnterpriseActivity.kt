package com.example.empresas_android.presentation.ui.enterprise

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.empresas_android.databinding.ActivityEnterpriseBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class EnterpriseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val bindingEnterprise = ActivityEnterpriseBinding.inflate(layoutInflater)
        setContentView(bindingEnterprise.root)

        setSupportActionBar(bindingEnterprise.toolbar)

    }

}