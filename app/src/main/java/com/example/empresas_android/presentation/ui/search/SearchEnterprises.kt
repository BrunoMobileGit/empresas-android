package com.example.empresas_android.presentation.ui.search

import android.os.Bundle
import android.view.*
import android.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.example.empresas_android.R
import com.example.empresas_android.databinding.FragmentSearchEnterpriseBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class SearchEnterprises : Fragment() {

    val adapterEnterprises by lazy { SearchEnterpriseAdapter() }

    val searchViewModel: SearchViewModel by viewModels()

    lateinit var _bindingSearchEnterprise: FragmentSearchEnterpriseBinding
    val bindingSearchEnterprise: FragmentSearchEnterpriseBinding get() = _bindingSearchEnterprise

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?

    ): View {

        _bindingSearchEnterprise =
            FragmentSearchEnterpriseBinding.inflate(inflater, container, false)

        setupRecyclerView()

        if (adapterEnterprises.itemCount == 0) {
            bindingSearchEnterprise.textInformation.visibility = View.VISIBLE
        } else {
            bindingSearchEnterprise.textInformation.visibility = View.INVISIBLE
        }

        searchViewModel.enterprises.observe(viewLifecycleOwner, { enterprises ->
            if (enterprises.isEmpty()) {
                bindingSearchEnterprise.textNoResult.visibility = View.VISIBLE
                adapterEnterprises.submitList(null)
            } else {
                adapterEnterprises.submitList(enterprises)
            }

        })

        setHasOptionsMenu(true)

        return bindingSearchEnterprise.root
    }

    fun setupRecyclerView() {
        bindingSearchEnterprise.recyclerViewEnterprise.adapter = adapterEnterprises
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_toolbar, menu)

        val search = menu.findItem(R.id.searchCompany)
        val searchView = search.actionView as androidx.appcompat.widget.SearchView

        searchView.queryHint = getString(R.string.menu_item_pesquisar)

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener,
            androidx.appcompat.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                lifecycleScope.launch {
                    searchViewModel.getEnterprise(query.toString())
                }
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                bindingSearchEnterprise.textInformation.visibility = View.INVISIBLE
                bindingSearchEnterprise.textNoResult.visibility = View.INVISIBLE
                return true
            }

        })

        super.onCreateOptionsMenu(menu, inflater)

    }


}