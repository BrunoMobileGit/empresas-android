package com.example.empresas_android.presentation.ui.search

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.example.empresas_android.data.model.Enterprise
import com.example.empresas_android.domain.util.Constants.Companion.BASE_URL_IMAGE

class SearchBindingAdapter {

    companion object {

        @BindingAdapter("loadImage")
        @JvmStatic
        fun loadImage(imageView: ImageView, enterprise: Enterprise) {
            var imageUrl = BASE_URL_IMAGE + enterprise.photo
            Glide.with(imageView)
                .load(imageUrl)
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(imageView)

        }
    }
}