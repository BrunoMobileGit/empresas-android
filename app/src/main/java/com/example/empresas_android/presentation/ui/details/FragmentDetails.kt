package com.example.empresas_android.presentation.ui.details

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import com.example.empresas_android.databinding.FragmentDetailsBinding

class FragmentDetails : Fragment() {

    private val args by navArgs<FragmentDetailsArgs>()

    lateinit var _binding: FragmentDetailsBinding
    val binding: FragmentDetailsBinding get() = _binding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentDetailsBinding.inflate(inflater, container, false)

        val enterprise = args.enterprise
        binding.enterprise = enterprise

        return binding.root

    }

}