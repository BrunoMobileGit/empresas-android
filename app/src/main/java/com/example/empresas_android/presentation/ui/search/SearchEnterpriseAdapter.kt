package com.example.empresas_android.presentation.ui.search

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.empresas_android.data.model.Enterprise
import com.example.empresas_android.databinding.RowEnterpriseItemBinding

class SearchEnterpriseAdapter :
    ListAdapter<Enterprise, SearchEnterpriseAdapter.EnterprisesViewHolder>(DiffUtilCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EnterprisesViewHolder {
        val binding =
            RowEnterpriseItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return EnterprisesViewHolder(binding)
    }

    override fun onBindViewHolder(holder: EnterprisesViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class EnterprisesViewHolder(private val binding: RowEnterpriseItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(enterprise: Enterprise) {
            binding.enterprise = enterprise

            binding.rowItemEnterprise.setOnClickListener {
                val action = SearchEnterprisesDirections.actionSearchCompanyToFragmentDetails2(enterprise)
                binding.rowItemEnterprise.findNavController().navigate(action)
            }
        }
    }
}

private object DiffUtilCallback : DiffUtil.ItemCallback<Enterprise>() {
    override fun areItemsTheSame(oldItem: Enterprise, newItem: Enterprise): Boolean =
        oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: Enterprise, newItem: Enterprise): Boolean =
        oldItem == newItem
}