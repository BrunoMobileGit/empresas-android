package com.example.empresas_android.domain.usecases

import com.example.empresas_android.domain.repository.Repository
import javax.inject.Inject

class PostLoginUseCase @Inject constructor(
    private val repository: Repository
) {

    suspend fun postLogin(email: String, password: String): Boolean =
        repository.postLogin(email, password)

}