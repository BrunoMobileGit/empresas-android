package com.example.empresas_android.domain.repository

import com.example.empresas_android.data.model.Enterprise

interface Repository {

    suspend fun postLogin(email: String, password: String): Boolean

    suspend fun getEnterprise(name: String): List<Enterprise>?

}