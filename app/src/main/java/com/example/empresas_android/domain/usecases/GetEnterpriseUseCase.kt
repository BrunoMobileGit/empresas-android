package com.example.empresas_android.domain.usecases

import com.example.empresas_android.data.model.Enterprise
import com.example.empresas_android.domain.repository.Repository
import javax.inject.Inject

class GetEnterpriseUseCase @Inject constructor(
    private val repository: Repository
) {

    suspend fun getEnterprise(name: String): List<Enterprise>? =
        repository.getEnterprise(name)

}